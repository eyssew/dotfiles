# Dotfiles
This is my dotfiles repo. The forest gruvbox repo is partially stolen from RMackner: https://github.com/RMackner/RMackner
The forest rice is my current rice and it's way more "ready" than my old sandy rice. The sandy rice is from a time I wasn't experienced at all.

# Scripts
In my forest-gruvbox-hyprland/hypr/scripts, you can find some pretty nice bash scripts that make a WM way more of a "desktop environment"

# Dependencies
- hyprland
- waybar
- foot
- paru
- pacman-contrib
- system-montoring-center
- hyprshot
- min
- nemo
- dunst
- swww
- hyprpicker
- gamemode
- cava

