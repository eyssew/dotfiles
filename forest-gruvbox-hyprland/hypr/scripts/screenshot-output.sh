#!/usr/bin/bash

# Slurping and getting it into a "variable"
slurp -o DP-3 > /home/rat/tmp-screenshot.txt

# Copying the screenshot
cat /home/rat/tmp-screenshot.txt | \
xargs -I % grim -g % - | wl-copy

# Saving the screenshot to ~/Pictures/
cat /home/rat/tmp-screenshot.txt | \
xargs -I % grim -g %
