#!/bin/sh

ACTIVEWORKSPACE=$(hyprctl -j monitors | jq '.[] | select(.focused == true) | .activeWorkspace.id')
LASTMIN=$(cat .tmp/minimized.txt | tail -1 | awk -F "," {'print $1'})
hyprctl dispatch movetoworkspacesilent "$ACTIVEWORKSPACE,address:0x$LASTMIN"
echo hyprctl dispatch movetoworkspacesilent $LASTMIN $ACTIVEWORKSPACE

