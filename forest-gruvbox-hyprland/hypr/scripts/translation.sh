#!/bin/sh

ftext=$(wl-paste)
flangn=$(trans :nl -no-ansi -identify "$ftext" | awk 'NR==1')
flangconv=$(echo "$flangn" | trans :nl -b )
translation=$(trans :nl -b "$ftext")
title=$(echo "Vertaald uit het $flangconv: ")

notify-send "$title" "$translation"
