#!/usr/bin/env bash

if [[ $(hyprctl getoption decoration:rounding | sed -n 2p | awk '{print $2, $3}') > *1* ]]; then
  exit 0
fi

killall waybar
hyprctl keyword decoration:rounding 8
hyprctl keyword decoration:drop_shadow 1
hyprctl keyword general:border_size 0
hyprctl keyword decoration:drop_shadow 1

swww img /usr/share/backgrounds/7h0bi4pf4om91.png --transition-fps 120
waybar -c /home/rat/.config/waybar/config.jsonc
sleep 1
waybar -c /home/rat/.config/waybar/config-links.jsonc
sleep 1
waybar -c /home/rat/.config/waybar/config-rechts.jsonc
