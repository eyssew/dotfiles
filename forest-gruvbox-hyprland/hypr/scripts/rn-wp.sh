#!/usr/bin/bash

ls -a /usr/share/backgrounds/ | \
shuf -n1 | \
xargs -I % swww img /usr/share/backgrounds/% \
--transition-type grow \
--transition-fps 60 \
--transition-pos 1070,1050
