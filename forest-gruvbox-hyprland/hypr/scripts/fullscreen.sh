#!/usr/bin/env bash

if [[ $(hyprctl getoption decoration:rounding | sed -n 2p | awk '{print $2, $3}') = *0* ]]; then
  exit 0
fi

pkill -9 waybar
waybar -s /home/rat/.config/waybar/fullscreen.css &
hyprctl keyword decoration:rounding 0
hyprctl keyword decoration:drop_shadow 0
hyprctl keyword general:border_size 1
swww img /usr/share/backgrounds/965fbe493765ac016bb06e26461b9530.png
