#!/bin/sh

function handle {
  if [[ ${1:0:8} == "minimize" ]]; then
    echo ${1:10:18} >> /home/rat/.tmp/minimized.txt
    hyprctl dispatch movetoworkspacesilent 50
  fi
}

socat - UNIX-CONNECT:/tmp/hypr/$(echo $HYPRLAND_INSTANCE_SIGNATURE)/.socket2.sock | while read line; do handle $line; done
